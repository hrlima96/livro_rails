class Room < ActiveRecord::Base
  validates_presence_of :title, :location, :description
  validates_length_of :description, :minimum => 30, :allow_blank => false
  validates_length_of :title, :minimum => 10, :maximum =>30

  def complete_name
    "#{title}, #{location}"
  end
end
